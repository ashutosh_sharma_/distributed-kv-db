package com.kvstore;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StoreOperationsTest {
    StoreOperations storeOperations;

    @BeforeAll
    public void setUp(){
        storeOperations = StoreOperations.getInstance();
    }

    @Test
    public void insertOperations(){
        String insertKey = "delhi";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("pollution_level", "very high");
        hashMap.put("population", "10 Million");
        Gson gson = new Gson();
        String attributeString = gson.toJson(hashMap);
        storeOperations.insertData(insertKey,attributeString);
        Assert.assertEquals("10 Million", storeOperations.searchByKey("delhi", "population"));
    }
}