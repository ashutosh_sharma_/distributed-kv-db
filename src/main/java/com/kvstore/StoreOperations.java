package com.kvstore;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

public class StoreOperations {
    private static StoreOperations storeOperationsInstance = null;

    private StoreOperations(){

    }

    public static StoreOperations getInstance(){
        if (storeOperationsInstance == null)
            storeOperationsInstance = new StoreOperations();

        return storeOperationsInstance;
    }

    //Creating a file based distributed kv store
    public void insertData(String key, String attributeString){
        int dbIndex = getDBIndex(key);
        String filePath = new File("").getAbsolutePath() + "/store-db/db" + dbIndex + "/" + key +".json";
        File file = new File(filePath);

        JsonObject attributeObj = new JsonParser().parse(attributeString).getAsJsonObject();
        try {
            if (!file.exists())
                file.createNewFile();

            FileWriter writer = new FileWriter(filePath);
            writer.write(attributeString);
            writer.close();

            for (String attributeName : attributeObj.keySet()){
                String attributePath = new File("").getAbsolutePath() + "/store-db/db-index/" + attributeName + "/" + attributeObj.get(attributeName).getAsString();
                File folder = new File(attributePath);
                if (!folder.exists())
                    folder.mkdirs();
                attributePath = attributePath + "/" + key;
                file = new File(attributePath);
                file.createNewFile();
            }
        }
        catch (IOException e){
            System.out.println("IO error occurred");
            e.printStackTrace();
        }
    }

    //Fetching data for a given key
    public String searchByKey(String key, String attributeName){
        int dbIndex = getDBIndex(key);
        String filePath = new File("").getAbsolutePath() + "/store-db/db" + dbIndex + "/" + key +".json";
        File file = new File(filePath);

        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        String response = "";
        Gson gson = new Gson();
        HashMap<String, String> attributeMap = null;
        try {
            if (file.exists()){
                JsonReader jsonReader = new JsonReader(new FileReader(filePath));
                attributeMap = gson.fromJson(jsonReader, type);
            }
        }
        catch (IOException e){
            System.out.println("IO error occurred");
            e.printStackTrace();
        }

        if (attributeMap != null && !attributeMap.isEmpty()) {
            if (attributeName == null || attributeName.trim().length() == 0)
                response = gson.toJson(attributeMap);
            else if (attributeName.trim().length() > 0)
                response = attributeMap.get(attributeName);
        }
        return response;
    }

    //Code for secondary index scan
    public  String searchByValue(String attributeName, String attributeValue){
        String attributePath = new File("").getAbsolutePath() + "/store-db/db-index/" + attributeName + "/" + attributeValue;
        File folder = new File(attributePath);
        String response = "";
        if (folder.exists()) {
            File[] keys = folder.listFiles();

            for (int i = 0; i < keys.length; i++)
                response += keys[i].getName() + ",";

            response = response.replaceAll(",$", "");
        }
        return response;
    }

    //Central logic deciding the database where the new entries should be made
    private int getDBIndex(String key){
        int keyHashCode = key.hashCode();

        if (keyHashCode % 2 == 0)
            return 2;
        else
            return 1;
    }
}
