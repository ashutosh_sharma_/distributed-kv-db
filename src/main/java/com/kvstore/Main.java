package com.kvstore;

import com.google.gson.Gson;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        //Gettign instance of the class where all the operations are performed
        StoreOperations storeOperations = StoreOperations.getInstance();

        //Inserting data
        String insertKey = "delhi";
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("pollution_level", "very high");
        hashMap.put("population", "10 Million");
        Gson gson = new Gson();
        String attributeString = gson.toJson(hashMap);
        storeOperations.insertData(insertKey,attributeString);
        hashMap.clear();

        insertKey = "jakarta";
        hashMap.put("Latitude","-6.0");
        hashMap.put("Longitude","106");
        hashMap.put("pollution_level","high");
        attributeString = gson.toJson(hashMap);
        storeOperations.insertData(insertKey, attributeString);
        hashMap.clear();

        //Search based on key
        String searchResult = "";
        searchResult = storeOperations.searchByKey("delhi", null);
        System.out.println(searchResult);

        //Search based on key for a specific attribute
        searchResult = storeOperations.searchByKey("delhi", "pollution_level");
        System.out.println(searchResult);

        //Second index scan
        searchResult = storeOperations.searchByValue("pollution_level", "high");
        System.out.println(searchResult);
    }
}
